<?php

namespace WsGsb;

use Zend\Mvc\MvcEvent;
use Zend\Session\Container;
use Zend\Http\Response;

class Module {

    public function onBootstrap(MvcEvent $e) {

        $eventManager = $e->getApplication()->getEventManager();
        $eventManager->attach(MvcEvent::EVENT_DISPATCH, function($e) {
            $session = new Container('utilisateur');
            $controllerName = $e->getRouteMatch()->getParam('controller');
            if ($controllerName != 'Connection' && $controllerName != 'Main' && $session->client == null) {
                $response = new Response();
                $response->setStatusCode(Response::STATUS_CODE_200);
                $response->setContent('Connexion nécessaire');
                $e->stopPropagation(true);
                return $response;
                ;
            }
        }, 100);

        $eventManager->attach(MvcEvent::EVENT_RENDER_ERROR, function ($e) {
            return array("error" =>var_dump( $e->getError()));
        });
    }

    public function getConfig() {
        require __DIR__ . '/config/propel.config.php';
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

}
