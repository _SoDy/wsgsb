<?php

$host = "localhost" ;
$dbName = "gsb";
$dsn = "mysql:host=$host;dbname=$dbName;charset=utf8";
$user = "sody";
$mdp = "root";

$serviceContainer = \Propel\Runtime\Propel::getServiceContainer();
$serviceContainer->checkVersion('2.0.0-dev');
$serviceContainer->setAdapterClass('WsGsbConnection', 'mysql');
$manager = new \Propel\Runtime\Connection\ConnectionManagerSingle();
$manager->setConfiguration(array(
    'classname' => 'Propel\\Runtime\\Connection\\ConnectionWrapper',
    'dsn' => $dsn,
    'user' => $user,
    'password' => $mdp,
    'attributes' =>
    array(
        'ATTR_EMULATE_PREPARES' => false,
        'ATTR_TIMEOUT' => 30,
    ),
    'model_paths' =>
    array(
        0 => 'src',
        1 => 'vendor',
    ),
));
$manager->setName('WsGsbConnection');
$serviceContainer->setConnectionManager('WsGsbConnection', $manager);
$serviceContainer->setDefaultDatasource('WsGsbConnection');






