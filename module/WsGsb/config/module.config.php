<?php

return array(
    'router' => array(
        'routes' => array(
            'root' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/appliFrais',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'WsGsb\Controller',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'rootChild' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/:controller',
                            'constraints' => array(
                                'controller' => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'WsGsb\Controller',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Connection' => 'WsGsb\Controller\ConnectionController',
            'FicheFrais' => 'WsGsb\Controller\FichefraisController',
            'FraisForfait' => 'WsGsb\Controller\FraisforfaitController',
            'LigneFraisForfait' => 'WsGsb\Controller\LignefraisforfaitController',
            'LigneFraisHorsForfait' => 'WsGsb\Controller\LignefraishorsforfaitController',
            'Etat' => 'WsGsb\Controller\EtatController',
            'User' => 'WsGsb\Controller\UserController',
            'Main'=>'WsGsb\Controller\MainController'
        ),
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),

);

