<?php

namespace WsGsb\Model\Map;

use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;
use WsGsb\Model\Fichefrais;
use WsGsb\Model\FichefraisQuery;


/**
 * This class defines the structure of the 'fichefrais' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class FichefraisTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'WsGsb.Model.Map.FichefraisTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'WsGsbConnection';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'fichefrais';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\WsGsb\\Model\\Fichefrais';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'WsGsb.Model.Fichefrais';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 7;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 7;

    /**
     * the column name for the idFicheFrais field
     */
    const COL_IDFICHEFRAIS = 'fichefrais.idFicheFrais';

    /**
     * the column name for the moisAnnee field
     */
    const COL_MOISANNEE = 'fichefrais.moisAnnee';

    /**
     * the column name for the nbJustificatifs field
     */
    const COL_NBJUSTIFICATIFS = 'fichefrais.nbJustificatifs';

    /**
     * the column name for the montantValide field
     */
    const COL_MONTANTVALIDE = 'fichefrais.montantValide';

    /**
     * the column name for the dateModif field
     */
    const COL_DATEMODIF = 'fichefrais.dateModif';

    /**
     * the column name for the idVisiteur field
     */
    const COL_IDVISITEUR = 'fichefrais.idVisiteur';

    /**
     * the column name for the idEtat field
     */
    const COL_IDETAT = 'fichefrais.idEtat';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Idfichefrais', 'Moisannee', 'Nbjustificatifs', 'Montantvalide', 'Datemodif', 'Idvisiteur', 'Idetat', ),
        self::TYPE_CAMELNAME     => array('idfichefrais', 'moisannee', 'nbjustificatifs', 'montantvalide', 'datemodif', 'idvisiteur', 'idetat', ),
        self::TYPE_COLNAME       => array(FichefraisTableMap::COL_IDFICHEFRAIS, FichefraisTableMap::COL_MOISANNEE, FichefraisTableMap::COL_NBJUSTIFICATIFS, FichefraisTableMap::COL_MONTANTVALIDE, FichefraisTableMap::COL_DATEMODIF, FichefraisTableMap::COL_IDVISITEUR, FichefraisTableMap::COL_IDETAT, ),
        self::TYPE_FIELDNAME     => array('idFicheFrais', 'moisAnnee', 'nbJustificatifs', 'montantValide', 'dateModif', 'idVisiteur', 'idEtat', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Idfichefrais' => 0, 'Moisannee' => 1, 'Nbjustificatifs' => 2, 'Montantvalide' => 3, 'Datemodif' => 4, 'Idvisiteur' => 5, 'Idetat' => 6, ),
        self::TYPE_CAMELNAME     => array('idfichefrais' => 0, 'moisannee' => 1, 'nbjustificatifs' => 2, 'montantvalide' => 3, 'datemodif' => 4, 'idvisiteur' => 5, 'idetat' => 6, ),
        self::TYPE_COLNAME       => array(FichefraisTableMap::COL_IDFICHEFRAIS => 0, FichefraisTableMap::COL_MOISANNEE => 1, FichefraisTableMap::COL_NBJUSTIFICATIFS => 2, FichefraisTableMap::COL_MONTANTVALIDE => 3, FichefraisTableMap::COL_DATEMODIF => 4, FichefraisTableMap::COL_IDVISITEUR => 5, FichefraisTableMap::COL_IDETAT => 6, ),
        self::TYPE_FIELDNAME     => array('idFicheFrais' => 0, 'moisAnnee' => 1, 'nbJustificatifs' => 2, 'montantValide' => 3, 'dateModif' => 4, 'idVisiteur' => 5, 'idEtat' => 6, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('fichefrais');
        $this->setPhpName('Fichefrais');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\WsGsb\\Model\\Fichefrais');
        $this->setPackage('WsGsb.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idFicheFrais', 'Idfichefrais', 'INTEGER', true, null, null);
        $this->addColumn('moisAnnee', 'Moisannee', 'CHAR', true, 6, null);
        $this->addColumn('nbJustificatifs', 'Nbjustificatifs', 'TINYINT', false, null, null);
        $this->addColumn('montantValide', 'Montantvalide', 'DECIMAL', false, null, null);
        $this->addColumn('dateModif', 'Datemodif', 'TIMESTAMP', false, null, null);
        $this->addForeignKey('idVisiteur', 'Idvisiteur', 'VARCHAR', 'user', 'idUser', true, 4, null);
        $this->addForeignKey('idEtat', 'Idetat', 'VARCHAR', 'etat', 'idEtat', true, 2, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Etat', '\\WsGsb\\Model\\Etat', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':idEtat',
    1 => ':idEtat',
  ),
), null, null, null, false);
        $this->addRelation('User', '\\WsGsb\\Model\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':idVisiteur',
    1 => ':idUser',
  ),
), null, null, null, false);
        $this->addRelation('Lignefraisforfait', '\\WsGsb\\Model\\Lignefraisforfait', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':idFicheFrais',
    1 => ':idFicheFrais',
  ),
), null, null, 'Lignefraisforfaits', false);
        $this->addRelation('Lignefraishorsforfait', '\\WsGsb\\Model\\Lignefraishorsforfait', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':idFicheFrais',
    1 => ':idFicheFrais',
  ),
), 'CASCADE', 'CASCADE', 'Lignefraishorsforfaits', false);
    } // buildRelations()
    /**
     * Method to invalidate the instance pool of all tables related to fichefrais     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        LignefraishorsforfaitTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfichefrais', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfichefrais', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfichefrais', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfichefrais', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfichefrais', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfichefrais', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idfichefrais', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? FichefraisTableMap::CLASS_DEFAULT : FichefraisTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Fichefrais object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = FichefraisTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = FichefraisTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + FichefraisTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = FichefraisTableMap::OM_CLASS;
            /** @var Fichefrais $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            FichefraisTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = FichefraisTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = FichefraisTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Fichefrais $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                FichefraisTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(FichefraisTableMap::COL_IDFICHEFRAIS);
            $criteria->addSelectColumn(FichefraisTableMap::COL_MOISANNEE);
            $criteria->addSelectColumn(FichefraisTableMap::COL_NBJUSTIFICATIFS);
            $criteria->addSelectColumn(FichefraisTableMap::COL_MONTANTVALIDE);
            $criteria->addSelectColumn(FichefraisTableMap::COL_DATEMODIF);
            $criteria->addSelectColumn(FichefraisTableMap::COL_IDVISITEUR);
            $criteria->addSelectColumn(FichefraisTableMap::COL_IDETAT);
        } else {
            $criteria->addSelectColumn($alias . '.idFicheFrais');
            $criteria->addSelectColumn($alias . '.moisAnnee');
            $criteria->addSelectColumn($alias . '.nbJustificatifs');
            $criteria->addSelectColumn($alias . '.montantValide');
            $criteria->addSelectColumn($alias . '.dateModif');
            $criteria->addSelectColumn($alias . '.idVisiteur');
            $criteria->addSelectColumn($alias . '.idEtat');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(FichefraisTableMap::DATABASE_NAME)->getTable(FichefraisTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(FichefraisTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(FichefraisTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new FichefraisTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Fichefrais or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Fichefrais object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FichefraisTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \WsGsb\Model\Fichefrais) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(FichefraisTableMap::DATABASE_NAME);
            $criteria->add(FichefraisTableMap::COL_IDFICHEFRAIS, (array) $values, Criteria::IN);
        }

        $query = FichefraisQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            FichefraisTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                FichefraisTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the fichefrais table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return FichefraisQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Fichefrais or Criteria object.
     *
     * @param mixed               $criteria Criteria or Fichefrais object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FichefraisTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Fichefrais object
        }

        if ($criteria->containsKey(FichefraisTableMap::COL_IDFICHEFRAIS) && $criteria->keyContainsValue(FichefraisTableMap::COL_IDFICHEFRAIS) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.FichefraisTableMap::COL_IDFICHEFRAIS.')');
        }


        // Set the correct dbName
        $query = FichefraisQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // FichefraisTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
FichefraisTableMap::buildTableMap();
