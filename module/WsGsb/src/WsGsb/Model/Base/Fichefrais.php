<?php

namespace WsGsb\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;
use WsGsb\Model\Etat as ChildEtat;
use WsGsb\Model\EtatQuery as ChildEtatQuery;
use WsGsb\Model\Fichefrais as ChildFichefrais;
use WsGsb\Model\FichefraisQuery as ChildFichefraisQuery;
use WsGsb\Model\Lignefraisforfait as ChildLignefraisforfait;
use WsGsb\Model\LignefraisforfaitQuery as ChildLignefraisforfaitQuery;
use WsGsb\Model\Lignefraishorsforfait as ChildLignefraishorsforfait;
use WsGsb\Model\LignefraishorsforfaitQuery as ChildLignefraishorsforfaitQuery;
use WsGsb\Model\User as ChildUser;
use WsGsb\Model\UserQuery as ChildUserQuery;
use WsGsb\Model\Map\FichefraisTableMap;
use WsGsb\Model\Map\LignefraisforfaitTableMap;
use WsGsb\Model\Map\LignefraishorsforfaitTableMap;

/**
 * Base class that represents a row from the 'fichefrais' table.
 *
 *
 *
 * @package    propel.generator.WsGsb.Model.Base
 */
abstract class Fichefrais implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\WsGsb\\Model\\Map\\FichefraisTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idfichefrais field.
     *
     * @var        int
     */
    protected $idfichefrais;

    /**
     * The value for the moisannee field.
     *
     * @var        string
     */
    protected $moisannee;

    /**
     * The value for the nbjustificatifs field.
     *
     * @var        int
     */
    protected $nbjustificatifs;

    /**
     * The value for the montantvalide field.
     *
     * @var        string
     */
    protected $montantvalide;

    /**
     * The value for the datemodif field.
     *
     * @var        DateTime
     */
    protected $datemodif;

    /**
     * The value for the idvisiteur field.
     *
     * @var        string
     */
    protected $idvisiteur;

    /**
     * The value for the idetat field.
     *
     * @var        string
     */
    protected $idetat;

    /**
     * @var        ChildEtat
     */
    protected $aEtat;

    /**
     * @var        ChildUser
     */
    protected $aUser;

    /**
     * @var        ObjectCollection|ChildLignefraisforfait[] Collection to store aggregation of ChildLignefraisforfait objects.
     */
    protected $collLignefraisforfaits;
    protected $collLignefraisforfaitsPartial;

    /**
     * @var        ObjectCollection|ChildLignefraishorsforfait[] Collection to store aggregation of ChildLignefraishorsforfait objects.
     */
    protected $collLignefraishorsforfaits;
    protected $collLignefraishorsforfaitsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildLignefraisforfait[]
     */
    protected $lignefraisforfaitsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildLignefraishorsforfait[]
     */
    protected $lignefraishorsforfaitsScheduledForDeletion = null;

    /**
     * Initializes internal state of WsGsb\Model\Base\Fichefrais object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Fichefrais</code> instance.  If
     * <code>obj</code> is an instance of <code>Fichefrais</code>, delegates to
     * <code>equals(Fichefrais)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Fichefrais The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idfichefrais] column value.
     *
     * @return int
     */
    public function getIdfichefrais()
    {
        return $this->idfichefrais;
    }

    /**
     * Get the [moisannee] column value.
     *
     * @return string
     */
    public function getMoisannee()
    {
        return $this->moisannee;
    }

    /**
     * Get the [nbjustificatifs] column value.
     *
     * @return int
     */
    public function getNbjustificatifs()
    {
        return $this->nbjustificatifs;
    }

    /**
     * Get the [montantvalide] column value.
     *
     * @return string
     */
    public function getMontantvalide()
    {
        return $this->montantvalide;
    }

    /**
     * Get the [optionally formatted] temporal [datemodif] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDatemodif($format = NULL)
    {
        if ($format === null) {
            return $this->datemodif;
        } else {
            return $this->datemodif instanceof \DateTimeInterface ? $this->datemodif->format($format) : null;
        }
    }

    /**
     * Get the [idvisiteur] column value.
     *
     * @return string
     */
    public function getIdvisiteur()
    {
        return $this->idvisiteur;
    }

    /**
     * Get the [idetat] column value.
     *
     * @return string
     */
    public function getIdetat()
    {
        return $this->idetat;
    }

    /**
     * Set the value of [idfichefrais] column.
     *
     * @param int $v new value
     * @return $this|\WsGsb\Model\Fichefrais The current object (for fluent API support)
     */
    public function setIdfichefrais($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idfichefrais !== $v) {
            $this->idfichefrais = $v;
            $this->modifiedColumns[FichefraisTableMap::COL_IDFICHEFRAIS] = true;
        }

        return $this;
    } // setIdfichefrais()

    /**
     * Set the value of [moisannee] column.
     *
     * @param string $v new value
     * @return $this|\WsGsb\Model\Fichefrais The current object (for fluent API support)
     */
    public function setMoisannee($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->moisannee !== $v) {
            $this->moisannee = $v;
            $this->modifiedColumns[FichefraisTableMap::COL_MOISANNEE] = true;
        }

        return $this;
    } // setMoisannee()

    /**
     * Set the value of [nbjustificatifs] column.
     *
     * @param int $v new value
     * @return $this|\WsGsb\Model\Fichefrais The current object (for fluent API support)
     */
    public function setNbjustificatifs($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->nbjustificatifs !== $v) {
            $this->nbjustificatifs = $v;
            $this->modifiedColumns[FichefraisTableMap::COL_NBJUSTIFICATIFS] = true;
        }

        return $this;
    } // setNbjustificatifs()

    /**
     * Set the value of [montantvalide] column.
     *
     * @param string $v new value
     * @return $this|\WsGsb\Model\Fichefrais The current object (for fluent API support)
     */
    public function setMontantvalide($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->montantvalide !== $v) {
            $this->montantvalide = $v;
            $this->modifiedColumns[FichefraisTableMap::COL_MONTANTVALIDE] = true;
        }

        return $this;
    } // setMontantvalide()

    /**
     * Sets the value of [datemodif] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\WsGsb\Model\Fichefrais The current object (for fluent API support)
     */
    public function setDatemodif($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->datemodif !== null || $dt !== null) {
            if ($this->datemodif === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->datemodif->format("Y-m-d H:i:s.u")) {
                $this->datemodif = $dt === null ? null : clone $dt;
                $this->modifiedColumns[FichefraisTableMap::COL_DATEMODIF] = true;
            }
        } // if either are not null

        return $this;
    } // setDatemodif()

    /**
     * Set the value of [idvisiteur] column.
     *
     * @param string $v new value
     * @return $this|\WsGsb\Model\Fichefrais The current object (for fluent API support)
     */
    public function setIdvisiteur($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->idvisiteur !== $v) {
            $this->idvisiteur = $v;
            $this->modifiedColumns[FichefraisTableMap::COL_IDVISITEUR] = true;
        }

        if ($this->aUser !== null && $this->aUser->getIduser() !== $v) {
            $this->aUser = null;
        }

        return $this;
    } // setIdvisiteur()

    /**
     * Set the value of [idetat] column.
     *
     * @param string $v new value
     * @return $this|\WsGsb\Model\Fichefrais The current object (for fluent API support)
     */
    public function setIdetat($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->idetat !== $v) {
            $this->idetat = $v;
            $this->modifiedColumns[FichefraisTableMap::COL_IDETAT] = true;
        }

        if ($this->aEtat !== null && $this->aEtat->getIdetat() !== $v) {
            $this->aEtat = null;
        }

        return $this;
    } // setIdetat()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : FichefraisTableMap::translateFieldName('Idfichefrais', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idfichefrais = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : FichefraisTableMap::translateFieldName('Moisannee', TableMap::TYPE_PHPNAME, $indexType)];
            $this->moisannee = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : FichefraisTableMap::translateFieldName('Nbjustificatifs', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nbjustificatifs = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : FichefraisTableMap::translateFieldName('Montantvalide', TableMap::TYPE_PHPNAME, $indexType)];
            $this->montantvalide = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : FichefraisTableMap::translateFieldName('Datemodif', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->datemodif = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : FichefraisTableMap::translateFieldName('Idvisiteur', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idvisiteur = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : FichefraisTableMap::translateFieldName('Idetat', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idetat = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 7; // 7 = FichefraisTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\WsGsb\\Model\\Fichefrais'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aUser !== null && $this->idvisiteur !== $this->aUser->getIduser()) {
            $this->aUser = null;
        }
        if ($this->aEtat !== null && $this->idetat !== $this->aEtat->getIdetat()) {
            $this->aEtat = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FichefraisTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildFichefraisQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aEtat = null;
            $this->aUser = null;
            $this->collLignefraisforfaits = null;

            $this->collLignefraishorsforfaits = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Fichefrais::setDeleted()
     * @see Fichefrais::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(FichefraisTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildFichefraisQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(FichefraisTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                FichefraisTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aEtat !== null) {
                if ($this->aEtat->isModified() || $this->aEtat->isNew()) {
                    $affectedRows += $this->aEtat->save($con);
                }
                $this->setEtat($this->aEtat);
            }

            if ($this->aUser !== null) {
                if ($this->aUser->isModified() || $this->aUser->isNew()) {
                    $affectedRows += $this->aUser->save($con);
                }
                $this->setUser($this->aUser);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->lignefraisforfaitsScheduledForDeletion !== null) {
                if (!$this->lignefraisforfaitsScheduledForDeletion->isEmpty()) {
                    \WsGsb\Model\LignefraisforfaitQuery::create()
                        ->filterByPrimaryKeys($this->lignefraisforfaitsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->lignefraisforfaitsScheduledForDeletion = null;
                }
            }

            if ($this->collLignefraisforfaits !== null) {
                foreach ($this->collLignefraisforfaits as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->lignefraishorsforfaitsScheduledForDeletion !== null) {
                if (!$this->lignefraishorsforfaitsScheduledForDeletion->isEmpty()) {
                    \WsGsb\Model\LignefraishorsforfaitQuery::create()
                        ->filterByPrimaryKeys($this->lignefraishorsforfaitsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->lignefraishorsforfaitsScheduledForDeletion = null;
                }
            }

            if ($this->collLignefraishorsforfaits !== null) {
                foreach ($this->collLignefraishorsforfaits as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[FichefraisTableMap::COL_IDFICHEFRAIS] = true;
        if (null !== $this->idfichefrais) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . FichefraisTableMap::COL_IDFICHEFRAIS . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(FichefraisTableMap::COL_IDFICHEFRAIS)) {
            $modifiedColumns[':p' . $index++]  = 'idFicheFrais';
        }
        if ($this->isColumnModified(FichefraisTableMap::COL_MOISANNEE)) {
            $modifiedColumns[':p' . $index++]  = 'moisAnnee';
        }
        if ($this->isColumnModified(FichefraisTableMap::COL_NBJUSTIFICATIFS)) {
            $modifiedColumns[':p' . $index++]  = 'nbJustificatifs';
        }
        if ($this->isColumnModified(FichefraisTableMap::COL_MONTANTVALIDE)) {
            $modifiedColumns[':p' . $index++]  = 'montantValide';
        }
        if ($this->isColumnModified(FichefraisTableMap::COL_DATEMODIF)) {
            $modifiedColumns[':p' . $index++]  = 'dateModif';
        }
        if ($this->isColumnModified(FichefraisTableMap::COL_IDVISITEUR)) {
            $modifiedColumns[':p' . $index++]  = 'idVisiteur';
        }
        if ($this->isColumnModified(FichefraisTableMap::COL_IDETAT)) {
            $modifiedColumns[':p' . $index++]  = 'idEtat';
        }

        $sql = sprintf(
            'INSERT INTO fichefrais (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idFicheFrais':
                        $stmt->bindValue($identifier, $this->idfichefrais, PDO::PARAM_INT);
                        break;
                    case 'moisAnnee':
                        $stmt->bindValue($identifier, $this->moisannee, PDO::PARAM_STR);
                        break;
                    case 'nbJustificatifs':
                        $stmt->bindValue($identifier, $this->nbjustificatifs, PDO::PARAM_INT);
                        break;
                    case 'montantValide':
                        $stmt->bindValue($identifier, $this->montantvalide, PDO::PARAM_STR);
                        break;
                    case 'dateModif':
                        $stmt->bindValue($identifier, $this->datemodif ? $this->datemodif->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'idVisiteur':
                        $stmt->bindValue($identifier, $this->idvisiteur, PDO::PARAM_STR);
                        break;
                    case 'idEtat':
                        $stmt->bindValue($identifier, $this->idetat, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdfichefrais($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = FichefraisTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdfichefrais();
                break;
            case 1:
                return $this->getMoisannee();
                break;
            case 2:
                return $this->getNbjustificatifs();
                break;
            case 3:
                return $this->getMontantvalide();
                break;
            case 4:
                return $this->getDatemodif();
                break;
            case 5:
                return $this->getIdvisiteur();
                break;
            case 6:
                return $this->getIdetat();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Fichefrais'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Fichefrais'][$this->hashCode()] = true;
        $keys = FichefraisTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdfichefrais(),
            $keys[1] => $this->getMoisannee(),
            $keys[2] => $this->getNbjustificatifs(),
            $keys[3] => $this->getMontantvalide(),
            $keys[4] => $this->getDatemodif(),
            $keys[5] => $this->getIdvisiteur(),
            $keys[6] => $this->getIdetat(),
        );
        if ($result[$keys[4]] instanceof \DateTimeInterface) {
            $result[$keys[4]] = $result[$keys[4]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aEtat) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'etat';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'etat';
                        break;
                    default:
                        $key = 'Etat';
                }

                $result[$key] = $this->aEtat->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUser) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'user';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user';
                        break;
                    default:
                        $key = 'User';
                }

                $result[$key] = $this->aUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collLignefraisforfaits) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'lignefraisforfaits';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'lignefraisforfaits';
                        break;
                    default:
                        $key = 'Lignefraisforfaits';
                }

                $result[$key] = $this->collLignefraisforfaits->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collLignefraishorsforfaits) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'lignefraishorsforfaits';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'lignefraishorsforfaits';
                        break;
                    default:
                        $key = 'Lignefraishorsforfaits';
                }

                $result[$key] = $this->collLignefraishorsforfaits->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\WsGsb\Model\Fichefrais
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = FichefraisTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\WsGsb\Model\Fichefrais
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdfichefrais($value);
                break;
            case 1:
                $this->setMoisannee($value);
                break;
            case 2:
                $this->setNbjustificatifs($value);
                break;
            case 3:
                $this->setMontantvalide($value);
                break;
            case 4:
                $this->setDatemodif($value);
                break;
            case 5:
                $this->setIdvisiteur($value);
                break;
            case 6:
                $this->setIdetat($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = FichefraisTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdfichefrais($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setMoisannee($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setNbjustificatifs($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setMontantvalide($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDatemodif($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setIdvisiteur($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setIdetat($arr[$keys[6]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\WsGsb\Model\Fichefrais The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(FichefraisTableMap::DATABASE_NAME);

        if ($this->isColumnModified(FichefraisTableMap::COL_IDFICHEFRAIS)) {
            $criteria->add(FichefraisTableMap::COL_IDFICHEFRAIS, $this->idfichefrais);
        }
        if ($this->isColumnModified(FichefraisTableMap::COL_MOISANNEE)) {
            $criteria->add(FichefraisTableMap::COL_MOISANNEE, $this->moisannee);
        }
        if ($this->isColumnModified(FichefraisTableMap::COL_NBJUSTIFICATIFS)) {
            $criteria->add(FichefraisTableMap::COL_NBJUSTIFICATIFS, $this->nbjustificatifs);
        }
        if ($this->isColumnModified(FichefraisTableMap::COL_MONTANTVALIDE)) {
            $criteria->add(FichefraisTableMap::COL_MONTANTVALIDE, $this->montantvalide);
        }
        if ($this->isColumnModified(FichefraisTableMap::COL_DATEMODIF)) {
            $criteria->add(FichefraisTableMap::COL_DATEMODIF, $this->datemodif);
        }
        if ($this->isColumnModified(FichefraisTableMap::COL_IDVISITEUR)) {
            $criteria->add(FichefraisTableMap::COL_IDVISITEUR, $this->idvisiteur);
        }
        if ($this->isColumnModified(FichefraisTableMap::COL_IDETAT)) {
            $criteria->add(FichefraisTableMap::COL_IDETAT, $this->idetat);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildFichefraisQuery::create();
        $criteria->add(FichefraisTableMap::COL_IDFICHEFRAIS, $this->idfichefrais);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdfichefrais();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdfichefrais();
    }

    /**
     * Generic method to set the primary key (idfichefrais column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdfichefrais($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdfichefrais();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \WsGsb\Model\Fichefrais (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setMoisannee($this->getMoisannee());
        $copyObj->setNbjustificatifs($this->getNbjustificatifs());
        $copyObj->setMontantvalide($this->getMontantvalide());
        $copyObj->setDatemodif($this->getDatemodif());
        $copyObj->setIdvisiteur($this->getIdvisiteur());
        $copyObj->setIdetat($this->getIdetat());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getLignefraisforfaits() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addLignefraisforfait($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getLignefraishorsforfaits() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addLignefraishorsforfait($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdfichefrais(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \WsGsb\Model\Fichefrais Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildEtat object.
     *
     * @param  ChildEtat $v
     * @return $this|\WsGsb\Model\Fichefrais The current object (for fluent API support)
     * @throws PropelException
     */
    public function setEtat(ChildEtat $v = null)
    {
        if ($v === null) {
            $this->setIdetat(NULL);
        } else {
            $this->setIdetat($v->getIdetat());
        }

        $this->aEtat = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildEtat object, it will not be re-added.
        if ($v !== null) {
            $v->addFichefrais($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildEtat object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildEtat The associated ChildEtat object.
     * @throws PropelException
     */
    public function getEtat(ConnectionInterface $con = null)
    {
        if ($this->aEtat === null && (($this->idetat !== "" && $this->idetat !== null))) {
            $this->aEtat = ChildEtatQuery::create()->findPk($this->idetat, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aEtat->addFichefraiss($this);
             */
        }

        return $this->aEtat;
    }

    /**
     * Declares an association between this object and a ChildUser object.
     *
     * @param  ChildUser $v
     * @return $this|\WsGsb\Model\Fichefrais The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUser(ChildUser $v = null)
    {
        if ($v === null) {
            $this->setIdvisiteur(NULL);
        } else {
            $this->setIdvisiteur($v->getIduser());
        }

        $this->aUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUser object, it will not be re-added.
        if ($v !== null) {
            $v->addFichefrais($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUser object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUser The associated ChildUser object.
     * @throws PropelException
     */
    public function getUser(ConnectionInterface $con = null)
    {
        if ($this->aUser === null && (($this->idvisiteur !== "" && $this->idvisiteur !== null))) {
            $this->aUser = ChildUserQuery::create()->findPk($this->idvisiteur, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUser->addFichefraiss($this);
             */
        }

        return $this->aUser;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Lignefraisforfait' == $relationName) {
            $this->initLignefraisforfaits();
            return;
        }
        if ('Lignefraishorsforfait' == $relationName) {
            $this->initLignefraishorsforfaits();
            return;
        }
    }

    /**
     * Clears out the collLignefraisforfaits collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addLignefraisforfaits()
     */
    public function clearLignefraisforfaits()
    {
        $this->collLignefraisforfaits = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collLignefraisforfaits collection loaded partially.
     */
    public function resetPartialLignefraisforfaits($v = true)
    {
        $this->collLignefraisforfaitsPartial = $v;
    }

    /**
     * Initializes the collLignefraisforfaits collection.
     *
     * By default this just sets the collLignefraisforfaits collection to an empty array (like clearcollLignefraisforfaits());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initLignefraisforfaits($overrideExisting = true)
    {
        if (null !== $this->collLignefraisforfaits && !$overrideExisting) {
            return;
        }

        $collectionClassName = LignefraisforfaitTableMap::getTableMap()->getCollectionClassName();

        $this->collLignefraisforfaits = new $collectionClassName;
        $this->collLignefraisforfaits->setModel('\WsGsb\Model\Lignefraisforfait');
    }

    /**
     * Gets an array of ChildLignefraisforfait objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildFichefrais is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildLignefraisforfait[] List of ChildLignefraisforfait objects
     * @throws PropelException
     */
    public function getLignefraisforfaits(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collLignefraisforfaitsPartial && !$this->isNew();
        if (null === $this->collLignefraisforfaits || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collLignefraisforfaits) {
                // return empty collection
                $this->initLignefraisforfaits();
            } else {
                $collLignefraisforfaits = ChildLignefraisforfaitQuery::create(null, $criteria)
                    ->filterByFichefrais($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collLignefraisforfaitsPartial && count($collLignefraisforfaits)) {
                        $this->initLignefraisforfaits(false);

                        foreach ($collLignefraisforfaits as $obj) {
                            if (false == $this->collLignefraisforfaits->contains($obj)) {
                                $this->collLignefraisforfaits->append($obj);
                            }
                        }

                        $this->collLignefraisforfaitsPartial = true;
                    }

                    return $collLignefraisforfaits;
                }

                if ($partial && $this->collLignefraisforfaits) {
                    foreach ($this->collLignefraisforfaits as $obj) {
                        if ($obj->isNew()) {
                            $collLignefraisforfaits[] = $obj;
                        }
                    }
                }

                $this->collLignefraisforfaits = $collLignefraisforfaits;
                $this->collLignefraisforfaitsPartial = false;
            }
        }

        return $this->collLignefraisforfaits;
    }

    /**
     * Sets a collection of ChildLignefraisforfait objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $lignefraisforfaits A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildFichefrais The current object (for fluent API support)
     */
    public function setLignefraisforfaits(Collection $lignefraisforfaits, ConnectionInterface $con = null)
    {
        /** @var ChildLignefraisforfait[] $lignefraisforfaitsToDelete */
        $lignefraisforfaitsToDelete = $this->getLignefraisforfaits(new Criteria(), $con)->diff($lignefraisforfaits);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->lignefraisforfaitsScheduledForDeletion = clone $lignefraisforfaitsToDelete;

        foreach ($lignefraisforfaitsToDelete as $lignefraisforfaitRemoved) {
            $lignefraisforfaitRemoved->setFichefrais(null);
        }

        $this->collLignefraisforfaits = null;
        foreach ($lignefraisforfaits as $lignefraisforfait) {
            $this->addLignefraisforfait($lignefraisforfait);
        }

        $this->collLignefraisforfaits = $lignefraisforfaits;
        $this->collLignefraisforfaitsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Lignefraisforfait objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Lignefraisforfait objects.
     * @throws PropelException
     */
    public function countLignefraisforfaits(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collLignefraisforfaitsPartial && !$this->isNew();
        if (null === $this->collLignefraisforfaits || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLignefraisforfaits) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getLignefraisforfaits());
            }

            $query = ChildLignefraisforfaitQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByFichefrais($this)
                ->count($con);
        }

        return count($this->collLignefraisforfaits);
    }

    /**
     * Method called to associate a ChildLignefraisforfait object to this object
     * through the ChildLignefraisforfait foreign key attribute.
     *
     * @param  ChildLignefraisforfait $l ChildLignefraisforfait
     * @return $this|\WsGsb\Model\Fichefrais The current object (for fluent API support)
     */
    public function addLignefraisforfait(ChildLignefraisforfait $l)
    {
        if ($this->collLignefraisforfaits === null) {
            $this->initLignefraisforfaits();
            $this->collLignefraisforfaitsPartial = true;
        }

        if (!$this->collLignefraisforfaits->contains($l)) {
            $this->doAddLignefraisforfait($l);

            if ($this->lignefraisforfaitsScheduledForDeletion and $this->lignefraisforfaitsScheduledForDeletion->contains($l)) {
                $this->lignefraisforfaitsScheduledForDeletion->remove($this->lignefraisforfaitsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildLignefraisforfait $lignefraisforfait The ChildLignefraisforfait object to add.
     */
    protected function doAddLignefraisforfait(ChildLignefraisforfait $lignefraisforfait)
    {
        $this->collLignefraisforfaits[]= $lignefraisforfait;
        $lignefraisforfait->setFichefrais($this);
    }

    /**
     * @param  ChildLignefraisforfait $lignefraisforfait The ChildLignefraisforfait object to remove.
     * @return $this|ChildFichefrais The current object (for fluent API support)
     */
    public function removeLignefraisforfait(ChildLignefraisforfait $lignefraisforfait)
    {
        if ($this->getLignefraisforfaits()->contains($lignefraisforfait)) {
            $pos = $this->collLignefraisforfaits->search($lignefraisforfait);
            $this->collLignefraisforfaits->remove($pos);
            if (null === $this->lignefraisforfaitsScheduledForDeletion) {
                $this->lignefraisforfaitsScheduledForDeletion = clone $this->collLignefraisforfaits;
                $this->lignefraisforfaitsScheduledForDeletion->clear();
            }
            $this->lignefraisforfaitsScheduledForDeletion[]= clone $lignefraisforfait;
            $lignefraisforfait->setFichefrais(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Fichefrais is new, it will return
     * an empty collection; or if this Fichefrais has previously
     * been saved, it will retrieve related Lignefraisforfaits from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Fichefrais.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildLignefraisforfait[] List of ChildLignefraisforfait objects
     */
    public function getLignefraisforfaitsJoinFraisforfait(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildLignefraisforfaitQuery::create(null, $criteria);
        $query->joinWith('Fraisforfait', $joinBehavior);

        return $this->getLignefraisforfaits($query, $con);
    }

    /**
     * Clears out the collLignefraishorsforfaits collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addLignefraishorsforfaits()
     */
    public function clearLignefraishorsforfaits()
    {
        $this->collLignefraishorsforfaits = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collLignefraishorsforfaits collection loaded partially.
     */
    public function resetPartialLignefraishorsforfaits($v = true)
    {
        $this->collLignefraishorsforfaitsPartial = $v;
    }

    /**
     * Initializes the collLignefraishorsforfaits collection.
     *
     * By default this just sets the collLignefraishorsforfaits collection to an empty array (like clearcollLignefraishorsforfaits());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initLignefraishorsforfaits($overrideExisting = true)
    {
        if (null !== $this->collLignefraishorsforfaits && !$overrideExisting) {
            return;
        }

        $collectionClassName = LignefraishorsforfaitTableMap::getTableMap()->getCollectionClassName();

        $this->collLignefraishorsforfaits = new $collectionClassName;
        $this->collLignefraishorsforfaits->setModel('\WsGsb\Model\Lignefraishorsforfait');
    }

    /**
     * Gets an array of ChildLignefraishorsforfait objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildFichefrais is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildLignefraishorsforfait[] List of ChildLignefraishorsforfait objects
     * @throws PropelException
     */
    public function getLignefraishorsforfaits(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collLignefraishorsforfaitsPartial && !$this->isNew();
        if (null === $this->collLignefraishorsforfaits || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collLignefraishorsforfaits) {
                // return empty collection
                $this->initLignefraishorsforfaits();
            } else {
                $collLignefraishorsforfaits = ChildLignefraishorsforfaitQuery::create(null, $criteria)
                    ->filterByFichefrais($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collLignefraishorsforfaitsPartial && count($collLignefraishorsforfaits)) {
                        $this->initLignefraishorsforfaits(false);

                        foreach ($collLignefraishorsforfaits as $obj) {
                            if (false == $this->collLignefraishorsforfaits->contains($obj)) {
                                $this->collLignefraishorsforfaits->append($obj);
                            }
                        }

                        $this->collLignefraishorsforfaitsPartial = true;
                    }

                    return $collLignefraishorsforfaits;
                }

                if ($partial && $this->collLignefraishorsforfaits) {
                    foreach ($this->collLignefraishorsforfaits as $obj) {
                        if ($obj->isNew()) {
                            $collLignefraishorsforfaits[] = $obj;
                        }
                    }
                }

                $this->collLignefraishorsforfaits = $collLignefraishorsforfaits;
                $this->collLignefraishorsforfaitsPartial = false;
            }
        }

        return $this->collLignefraishorsforfaits;
    }

    /**
     * Sets a collection of ChildLignefraishorsforfait objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $lignefraishorsforfaits A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildFichefrais The current object (for fluent API support)
     */
    public function setLignefraishorsforfaits(Collection $lignefraishorsforfaits, ConnectionInterface $con = null)
    {
        /** @var ChildLignefraishorsforfait[] $lignefraishorsforfaitsToDelete */
        $lignefraishorsforfaitsToDelete = $this->getLignefraishorsforfaits(new Criteria(), $con)->diff($lignefraishorsforfaits);


        $this->lignefraishorsforfaitsScheduledForDeletion = $lignefraishorsforfaitsToDelete;

        foreach ($lignefraishorsforfaitsToDelete as $lignefraishorsforfaitRemoved) {
            $lignefraishorsforfaitRemoved->setFichefrais(null);
        }

        $this->collLignefraishorsforfaits = null;
        foreach ($lignefraishorsforfaits as $lignefraishorsforfait) {
            $this->addLignefraishorsforfait($lignefraishorsforfait);
        }

        $this->collLignefraishorsforfaits = $lignefraishorsforfaits;
        $this->collLignefraishorsforfaitsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Lignefraishorsforfait objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Lignefraishorsforfait objects.
     * @throws PropelException
     */
    public function countLignefraishorsforfaits(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collLignefraishorsforfaitsPartial && !$this->isNew();
        if (null === $this->collLignefraishorsforfaits || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLignefraishorsforfaits) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getLignefraishorsforfaits());
            }

            $query = ChildLignefraishorsforfaitQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByFichefrais($this)
                ->count($con);
        }

        return count($this->collLignefraishorsforfaits);
    }

    /**
     * Method called to associate a ChildLignefraishorsforfait object to this object
     * through the ChildLignefraishorsforfait foreign key attribute.
     *
     * @param  ChildLignefraishorsforfait $l ChildLignefraishorsforfait
     * @return $this|\WsGsb\Model\Fichefrais The current object (for fluent API support)
     */
    public function addLignefraishorsforfait(ChildLignefraishorsforfait $l)
    {
        if ($this->collLignefraishorsforfaits === null) {
            $this->initLignefraishorsforfaits();
            $this->collLignefraishorsforfaitsPartial = true;
        }

        if (!$this->collLignefraishorsforfaits->contains($l)) {
            $this->doAddLignefraishorsforfait($l);

            if ($this->lignefraishorsforfaitsScheduledForDeletion and $this->lignefraishorsforfaitsScheduledForDeletion->contains($l)) {
                $this->lignefraishorsforfaitsScheduledForDeletion->remove($this->lignefraishorsforfaitsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildLignefraishorsforfait $lignefraishorsforfait The ChildLignefraishorsforfait object to add.
     */
    protected function doAddLignefraishorsforfait(ChildLignefraishorsforfait $lignefraishorsforfait)
    {
        $this->collLignefraishorsforfaits[]= $lignefraishorsforfait;
        $lignefraishorsforfait->setFichefrais($this);
    }

    /**
     * @param  ChildLignefraishorsforfait $lignefraishorsforfait The ChildLignefraishorsforfait object to remove.
     * @return $this|ChildFichefrais The current object (for fluent API support)
     */
    public function removeLignefraishorsforfait(ChildLignefraishorsforfait $lignefraishorsforfait)
    {
        if ($this->getLignefraishorsforfaits()->contains($lignefraishorsforfait)) {
            $pos = $this->collLignefraishorsforfaits->search($lignefraishorsforfait);
            $this->collLignefraishorsforfaits->remove($pos);
            if (null === $this->lignefraishorsforfaitsScheduledForDeletion) {
                $this->lignefraishorsforfaitsScheduledForDeletion = clone $this->collLignefraishorsforfaits;
                $this->lignefraishorsforfaitsScheduledForDeletion->clear();
            }
            $this->lignefraishorsforfaitsScheduledForDeletion[]= clone $lignefraishorsforfait;
            $lignefraishorsforfait->setFichefrais(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aEtat) {
            $this->aEtat->removeFichefrais($this);
        }
        if (null !== $this->aUser) {
            $this->aUser->removeFichefrais($this);
        }
        $this->idfichefrais = null;
        $this->moisannee = null;
        $this->nbjustificatifs = null;
        $this->montantvalide = null;
        $this->datemodif = null;
        $this->idvisiteur = null;
        $this->idetat = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collLignefraisforfaits) {
                foreach ($this->collLignefraisforfaits as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collLignefraishorsforfaits) {
                foreach ($this->collLignefraishorsforfaits as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collLignefraisforfaits = null;
        $this->collLignefraishorsforfaits = null;
        $this->aEtat = null;
        $this->aUser = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(FichefraisTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
