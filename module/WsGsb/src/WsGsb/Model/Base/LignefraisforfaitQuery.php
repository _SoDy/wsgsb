<?php

namespace WsGsb\Model\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use WsGsb\Model\Lignefraisforfait as ChildLignefraisforfait;
use WsGsb\Model\LignefraisforfaitQuery as ChildLignefraisforfaitQuery;
use WsGsb\Model\Map\LignefraisforfaitTableMap;

/**
 * Base class that represents a query for the 'lignefraisforfait' table.
 *
 *
 *
 * @method     ChildLignefraisforfaitQuery orderByIdfraisforfait($order = Criteria::ASC) Order by the idFraisForfait column
 * @method     ChildLignefraisforfaitQuery orderByIdfichefrais($order = Criteria::ASC) Order by the idFicheFrais column
 * @method     ChildLignefraisforfaitQuery orderByQuantite($order = Criteria::ASC) Order by the quantite column
 *
 * @method     ChildLignefraisforfaitQuery groupByIdfraisforfait() Group by the idFraisForfait column
 * @method     ChildLignefraisforfaitQuery groupByIdfichefrais() Group by the idFicheFrais column
 * @method     ChildLignefraisforfaitQuery groupByQuantite() Group by the quantite column
 *
 * @method     ChildLignefraisforfaitQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildLignefraisforfaitQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildLignefraisforfaitQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildLignefraisforfaitQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildLignefraisforfaitQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildLignefraisforfaitQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildLignefraisforfaitQuery leftJoinFichefrais($relationAlias = null) Adds a LEFT JOIN clause to the query using the Fichefrais relation
 * @method     ChildLignefraisforfaitQuery rightJoinFichefrais($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Fichefrais relation
 * @method     ChildLignefraisforfaitQuery innerJoinFichefrais($relationAlias = null) Adds a INNER JOIN clause to the query using the Fichefrais relation
 *
 * @method     ChildLignefraisforfaitQuery joinWithFichefrais($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Fichefrais relation
 *
 * @method     ChildLignefraisforfaitQuery leftJoinWithFichefrais() Adds a LEFT JOIN clause and with to the query using the Fichefrais relation
 * @method     ChildLignefraisforfaitQuery rightJoinWithFichefrais() Adds a RIGHT JOIN clause and with to the query using the Fichefrais relation
 * @method     ChildLignefraisforfaitQuery innerJoinWithFichefrais() Adds a INNER JOIN clause and with to the query using the Fichefrais relation
 *
 * @method     ChildLignefraisforfaitQuery leftJoinFraisforfait($relationAlias = null) Adds a LEFT JOIN clause to the query using the Fraisforfait relation
 * @method     ChildLignefraisforfaitQuery rightJoinFraisforfait($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Fraisforfait relation
 * @method     ChildLignefraisforfaitQuery innerJoinFraisforfait($relationAlias = null) Adds a INNER JOIN clause to the query using the Fraisforfait relation
 *
 * @method     ChildLignefraisforfaitQuery joinWithFraisforfait($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Fraisforfait relation
 *
 * @method     ChildLignefraisforfaitQuery leftJoinWithFraisforfait() Adds a LEFT JOIN clause and with to the query using the Fraisforfait relation
 * @method     ChildLignefraisforfaitQuery rightJoinWithFraisforfait() Adds a RIGHT JOIN clause and with to the query using the Fraisforfait relation
 * @method     ChildLignefraisforfaitQuery innerJoinWithFraisforfait() Adds a INNER JOIN clause and with to the query using the Fraisforfait relation
 *
 * @method     \WsGsb\Model\FichefraisQuery|\WsGsb\Model\FraisforfaitQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildLignefraisforfait findOne(ConnectionInterface $con = null) Return the first ChildLignefraisforfait matching the query
 * @method     ChildLignefraisforfait findOneOrCreate(ConnectionInterface $con = null) Return the first ChildLignefraisforfait matching the query, or a new ChildLignefraisforfait object populated from the query conditions when no match is found
 *
 * @method     ChildLignefraisforfait findOneByIdfraisforfait(string $idFraisForfait) Return the first ChildLignefraisforfait filtered by the idFraisForfait column
 * @method     ChildLignefraisforfait findOneByIdfichefrais(int $idFicheFrais) Return the first ChildLignefraisforfait filtered by the idFicheFrais column
 * @method     ChildLignefraisforfait findOneByQuantite(int $quantite) Return the first ChildLignefraisforfait filtered by the quantite column *

 * @method     ChildLignefraisforfait requirePk($key, ConnectionInterface $con = null) Return the ChildLignefraisforfait by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLignefraisforfait requireOne(ConnectionInterface $con = null) Return the first ChildLignefraisforfait matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLignefraisforfait requireOneByIdfraisforfait(string $idFraisForfait) Return the first ChildLignefraisforfait filtered by the idFraisForfait column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLignefraisforfait requireOneByIdfichefrais(int $idFicheFrais) Return the first ChildLignefraisforfait filtered by the idFicheFrais column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLignefraisforfait requireOneByQuantite(int $quantite) Return the first ChildLignefraisforfait filtered by the quantite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLignefraisforfait[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildLignefraisforfait objects based on current ModelCriteria
 * @method     ChildLignefraisforfait[]|ObjectCollection findByIdfraisforfait(string $idFraisForfait) Return ChildLignefraisforfait objects filtered by the idFraisForfait column
 * @method     ChildLignefraisforfait[]|ObjectCollection findByIdfichefrais(int $idFicheFrais) Return ChildLignefraisforfait objects filtered by the idFicheFrais column
 * @method     ChildLignefraisforfait[]|ObjectCollection findByQuantite(int $quantite) Return ChildLignefraisforfait objects filtered by the quantite column
 * @method     ChildLignefraisforfait[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class LignefraisforfaitQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \WsGsb\Model\Base\LignefraisforfaitQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'WsGsbConnection', $modelName = '\\WsGsb\\Model\\Lignefraisforfait', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildLignefraisforfaitQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildLignefraisforfaitQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildLignefraisforfaitQuery) {
            return $criteria;
        }
        $query = new ChildLignefraisforfaitQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array[$idFraisForfait, $idFicheFrais] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildLignefraisforfait|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(LignefraisforfaitTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = LignefraisforfaitTableMap::getInstanceFromPool(serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]))))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLignefraisforfait A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idFraisForfait, idFicheFrais, quantite FROM lignefraisforfait WHERE idFraisForfait = :p0 AND idFicheFrais = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_STR);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildLignefraisforfait $obj */
            $obj = new ChildLignefraisforfait();
            $obj->hydrate($row);
            LignefraisforfaitTableMap::addInstanceToPool($obj, serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildLignefraisforfait|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildLignefraisforfaitQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(LignefraisforfaitTableMap::COL_IDFRAISFORFAIT, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(LignefraisforfaitTableMap::COL_IDFICHEFRAIS, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildLignefraisforfaitQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(LignefraisforfaitTableMap::COL_IDFRAISFORFAIT, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(LignefraisforfaitTableMap::COL_IDFICHEFRAIS, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the idFraisForfait column
     *
     * Example usage:
     * <code>
     * $query->filterByIdfraisforfait('fooValue');   // WHERE idFraisForfait = 'fooValue'
     * $query->filterByIdfraisforfait('%fooValue%', Criteria::LIKE); // WHERE idFraisForfait LIKE '%fooValue%'
     * </code>
     *
     * @param     string $idfraisforfait The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLignefraisforfaitQuery The current query, for fluid interface
     */
    public function filterByIdfraisforfait($idfraisforfait = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($idfraisforfait)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LignefraisforfaitTableMap::COL_IDFRAISFORFAIT, $idfraisforfait, $comparison);
    }

    /**
     * Filter the query on the idFicheFrais column
     *
     * Example usage:
     * <code>
     * $query->filterByIdfichefrais(1234); // WHERE idFicheFrais = 1234
     * $query->filterByIdfichefrais(array(12, 34)); // WHERE idFicheFrais IN (12, 34)
     * $query->filterByIdfichefrais(array('min' => 12)); // WHERE idFicheFrais > 12
     * </code>
     *
     * @see       filterByFichefrais()
     *
     * @param     mixed $idfichefrais The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLignefraisforfaitQuery The current query, for fluid interface
     */
    public function filterByIdfichefrais($idfichefrais = null, $comparison = null)
    {
        if (is_array($idfichefrais)) {
            $useMinMax = false;
            if (isset($idfichefrais['min'])) {
                $this->addUsingAlias(LignefraisforfaitTableMap::COL_IDFICHEFRAIS, $idfichefrais['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idfichefrais['max'])) {
                $this->addUsingAlias(LignefraisforfaitTableMap::COL_IDFICHEFRAIS, $idfichefrais['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LignefraisforfaitTableMap::COL_IDFICHEFRAIS, $idfichefrais, $comparison);
    }

    /**
     * Filter the query on the quantite column
     *
     * Example usage:
     * <code>
     * $query->filterByQuantite(1234); // WHERE quantite = 1234
     * $query->filterByQuantite(array(12, 34)); // WHERE quantite IN (12, 34)
     * $query->filterByQuantite(array('min' => 12)); // WHERE quantite > 12
     * </code>
     *
     * @param     mixed $quantite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLignefraisforfaitQuery The current query, for fluid interface
     */
    public function filterByQuantite($quantite = null, $comparison = null)
    {
        if (is_array($quantite)) {
            $useMinMax = false;
            if (isset($quantite['min'])) {
                $this->addUsingAlias(LignefraisforfaitTableMap::COL_QUANTITE, $quantite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($quantite['max'])) {
                $this->addUsingAlias(LignefraisforfaitTableMap::COL_QUANTITE, $quantite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LignefraisforfaitTableMap::COL_QUANTITE, $quantite, $comparison);
    }

    /**
     * Filter the query by a related \WsGsb\Model\Fichefrais object
     *
     * @param \WsGsb\Model\Fichefrais|ObjectCollection $fichefrais The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLignefraisforfaitQuery The current query, for fluid interface
     */
    public function filterByFichefrais($fichefrais, $comparison = null)
    {
        if ($fichefrais instanceof \WsGsb\Model\Fichefrais) {
            return $this
                ->addUsingAlias(LignefraisforfaitTableMap::COL_IDFICHEFRAIS, $fichefrais->getIdfichefrais(), $comparison);
        } elseif ($fichefrais instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LignefraisforfaitTableMap::COL_IDFICHEFRAIS, $fichefrais->toKeyValue('PrimaryKey', 'Idfichefrais'), $comparison);
        } else {
            throw new PropelException('filterByFichefrais() only accepts arguments of type \WsGsb\Model\Fichefrais or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Fichefrais relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLignefraisforfaitQuery The current query, for fluid interface
     */
    public function joinFichefrais($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Fichefrais');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Fichefrais');
        }

        return $this;
    }

    /**
     * Use the Fichefrais relation Fichefrais object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \WsGsb\Model\FichefraisQuery A secondary query class using the current class as primary query
     */
    public function useFichefraisQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFichefrais($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Fichefrais', '\WsGsb\Model\FichefraisQuery');
    }

    /**
     * Filter the query by a related \WsGsb\Model\Fraisforfait object
     *
     * @param \WsGsb\Model\Fraisforfait|ObjectCollection $fraisforfait The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLignefraisforfaitQuery The current query, for fluid interface
     */
    public function filterByFraisforfait($fraisforfait, $comparison = null)
    {
        if ($fraisforfait instanceof \WsGsb\Model\Fraisforfait) {
            return $this
                ->addUsingAlias(LignefraisforfaitTableMap::COL_IDFRAISFORFAIT, $fraisforfait->getIdfraisforfait(), $comparison);
        } elseif ($fraisforfait instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LignefraisforfaitTableMap::COL_IDFRAISFORFAIT, $fraisforfait->toKeyValue('PrimaryKey', 'Idfraisforfait'), $comparison);
        } else {
            throw new PropelException('filterByFraisforfait() only accepts arguments of type \WsGsb\Model\Fraisforfait or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Fraisforfait relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLignefraisforfaitQuery The current query, for fluid interface
     */
    public function joinFraisforfait($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Fraisforfait');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Fraisforfait');
        }

        return $this;
    }

    /**
     * Use the Fraisforfait relation Fraisforfait object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \WsGsb\Model\FraisforfaitQuery A secondary query class using the current class as primary query
     */
    public function useFraisforfaitQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFraisforfait($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Fraisforfait', '\WsGsb\Model\FraisforfaitQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildLignefraisforfait $lignefraisforfait Object to remove from the list of results
     *
     * @return $this|ChildLignefraisforfaitQuery The current query, for fluid interface
     */
    public function prune($lignefraisforfait = null)
    {
        if ($lignefraisforfait) {
            $this->addCond('pruneCond0', $this->getAliasedColName(LignefraisforfaitTableMap::COL_IDFRAISFORFAIT), $lignefraisforfait->getIdfraisforfait(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(LignefraisforfaitTableMap::COL_IDFICHEFRAIS), $lignefraisforfait->getIdfichefrais(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the lignefraisforfait table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LignefraisforfaitTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            LignefraisforfaitTableMap::clearInstancePool();
            LignefraisforfaitTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LignefraisforfaitTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(LignefraisforfaitTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            LignefraisforfaitTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            LignefraisforfaitTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // LignefraisforfaitQuery
