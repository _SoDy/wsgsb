<?php

namespace WsGsb\Model\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use WsGsb\Model\Fraisforfait as ChildFraisforfait;
use WsGsb\Model\FraisforfaitQuery as ChildFraisforfaitQuery;
use WsGsb\Model\Map\FraisforfaitTableMap;

/**
 * Base class that represents a query for the 'fraisforfait' table.
 *
 *
 *
 * @method     ChildFraisforfaitQuery orderByIdfraisforfait($order = Criteria::ASC) Order by the idFraisForfait column
 * @method     ChildFraisforfaitQuery orderByLibellefraisforfait($order = Criteria::ASC) Order by the libelleFraisForfait column
 * @method     ChildFraisforfaitQuery orderByMontantfraisforfait($order = Criteria::ASC) Order by the montantFraisForfait column
 *
 * @method     ChildFraisforfaitQuery groupByIdfraisforfait() Group by the idFraisForfait column
 * @method     ChildFraisforfaitQuery groupByLibellefraisforfait() Group by the libelleFraisForfait column
 * @method     ChildFraisforfaitQuery groupByMontantfraisforfait() Group by the montantFraisForfait column
 *
 * @method     ChildFraisforfaitQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildFraisforfaitQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildFraisforfaitQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildFraisforfaitQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildFraisforfaitQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildFraisforfaitQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildFraisforfaitQuery leftJoinLignefraisforfait($relationAlias = null) Adds a LEFT JOIN clause to the query using the Lignefraisforfait relation
 * @method     ChildFraisforfaitQuery rightJoinLignefraisforfait($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Lignefraisforfait relation
 * @method     ChildFraisforfaitQuery innerJoinLignefraisforfait($relationAlias = null) Adds a INNER JOIN clause to the query using the Lignefraisforfait relation
 *
 * @method     ChildFraisforfaitQuery joinWithLignefraisforfait($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Lignefraisforfait relation
 *
 * @method     ChildFraisforfaitQuery leftJoinWithLignefraisforfait() Adds a LEFT JOIN clause and with to the query using the Lignefraisforfait relation
 * @method     ChildFraisforfaitQuery rightJoinWithLignefraisforfait() Adds a RIGHT JOIN clause and with to the query using the Lignefraisforfait relation
 * @method     ChildFraisforfaitQuery innerJoinWithLignefraisforfait() Adds a INNER JOIN clause and with to the query using the Lignefraisforfait relation
 *
 * @method     \WsGsb\Model\LignefraisforfaitQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildFraisforfait findOne(ConnectionInterface $con = null) Return the first ChildFraisforfait matching the query
 * @method     ChildFraisforfait findOneOrCreate(ConnectionInterface $con = null) Return the first ChildFraisforfait matching the query, or a new ChildFraisforfait object populated from the query conditions when no match is found
 *
 * @method     ChildFraisforfait findOneByIdfraisforfait(string $idFraisForfait) Return the first ChildFraisforfait filtered by the idFraisForfait column
 * @method     ChildFraisforfait findOneByLibellefraisforfait(string $libelleFraisForfait) Return the first ChildFraisforfait filtered by the libelleFraisForfait column
 * @method     ChildFraisforfait findOneByMontantfraisforfait(string $montantFraisForfait) Return the first ChildFraisforfait filtered by the montantFraisForfait column *

 * @method     ChildFraisforfait requirePk($key, ConnectionInterface $con = null) Return the ChildFraisforfait by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFraisforfait requireOne(ConnectionInterface $con = null) Return the first ChildFraisforfait matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFraisforfait requireOneByIdfraisforfait(string $idFraisForfait) Return the first ChildFraisforfait filtered by the idFraisForfait column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFraisforfait requireOneByLibellefraisforfait(string $libelleFraisForfait) Return the first ChildFraisforfait filtered by the libelleFraisForfait column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFraisforfait requireOneByMontantfraisforfait(string $montantFraisForfait) Return the first ChildFraisforfait filtered by the montantFraisForfait column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFraisforfait[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildFraisforfait objects based on current ModelCriteria
 * @method     ChildFraisforfait[]|ObjectCollection findByIdfraisforfait(string $idFraisForfait) Return ChildFraisforfait objects filtered by the idFraisForfait column
 * @method     ChildFraisforfait[]|ObjectCollection findByLibellefraisforfait(string $libelleFraisForfait) Return ChildFraisforfait objects filtered by the libelleFraisForfait column
 * @method     ChildFraisforfait[]|ObjectCollection findByMontantfraisforfait(string $montantFraisForfait) Return ChildFraisforfait objects filtered by the montantFraisForfait column
 * @method     ChildFraisforfait[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class FraisforfaitQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \WsGsb\Model\Base\FraisforfaitQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'WsGsbConnection', $modelName = '\\WsGsb\\Model\\Fraisforfait', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildFraisforfaitQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildFraisforfaitQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildFraisforfaitQuery) {
            return $criteria;
        }
        $query = new ChildFraisforfaitQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildFraisforfait|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FraisforfaitTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = FraisforfaitTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFraisforfait A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idFraisForfait, libelleFraisForfait, montantFraisForfait FROM fraisforfait WHERE idFraisForfait = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildFraisforfait $obj */
            $obj = new ChildFraisforfait();
            $obj->hydrate($row);
            FraisforfaitTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildFraisforfait|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildFraisforfaitQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FraisforfaitTableMap::COL_IDFRAISFORFAIT, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildFraisforfaitQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FraisforfaitTableMap::COL_IDFRAISFORFAIT, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idFraisForfait column
     *
     * Example usage:
     * <code>
     * $query->filterByIdfraisforfait('fooValue');   // WHERE idFraisForfait = 'fooValue'
     * $query->filterByIdfraisforfait('%fooValue%', Criteria::LIKE); // WHERE idFraisForfait LIKE '%fooValue%'
     * </code>
     *
     * @param     string $idfraisforfait The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFraisforfaitQuery The current query, for fluid interface
     */
    public function filterByIdfraisforfait($idfraisforfait = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($idfraisforfait)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FraisforfaitTableMap::COL_IDFRAISFORFAIT, $idfraisforfait, $comparison);
    }

    /**
     * Filter the query on the libelleFraisForfait column
     *
     * Example usage:
     * <code>
     * $query->filterByLibellefraisforfait('fooValue');   // WHERE libelleFraisForfait = 'fooValue'
     * $query->filterByLibellefraisforfait('%fooValue%', Criteria::LIKE); // WHERE libelleFraisForfait LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libellefraisforfait The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFraisforfaitQuery The current query, for fluid interface
     */
    public function filterByLibellefraisforfait($libellefraisforfait = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libellefraisforfait)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FraisforfaitTableMap::COL_LIBELLEFRAISFORFAIT, $libellefraisforfait, $comparison);
    }

    /**
     * Filter the query on the montantFraisForfait column
     *
     * Example usage:
     * <code>
     * $query->filterByMontantfraisforfait(1234); // WHERE montantFraisForfait = 1234
     * $query->filterByMontantfraisforfait(array(12, 34)); // WHERE montantFraisForfait IN (12, 34)
     * $query->filterByMontantfraisforfait(array('min' => 12)); // WHERE montantFraisForfait > 12
     * </code>
     *
     * @param     mixed $montantfraisforfait The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFraisforfaitQuery The current query, for fluid interface
     */
    public function filterByMontantfraisforfait($montantfraisforfait = null, $comparison = null)
    {
        if (is_array($montantfraisforfait)) {
            $useMinMax = false;
            if (isset($montantfraisforfait['min'])) {
                $this->addUsingAlias(FraisforfaitTableMap::COL_MONTANTFRAISFORFAIT, $montantfraisforfait['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($montantfraisforfait['max'])) {
                $this->addUsingAlias(FraisforfaitTableMap::COL_MONTANTFRAISFORFAIT, $montantfraisforfait['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FraisforfaitTableMap::COL_MONTANTFRAISFORFAIT, $montantfraisforfait, $comparison);
    }

    /**
     * Filter the query by a related \WsGsb\Model\Lignefraisforfait object
     *
     * @param \WsGsb\Model\Lignefraisforfait|ObjectCollection $lignefraisforfait the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFraisforfaitQuery The current query, for fluid interface
     */
    public function filterByLignefraisforfait($lignefraisforfait, $comparison = null)
    {
        if ($lignefraisforfait instanceof \WsGsb\Model\Lignefraisforfait) {
            return $this
                ->addUsingAlias(FraisforfaitTableMap::COL_IDFRAISFORFAIT, $lignefraisforfait->getIdfraisforfait(), $comparison);
        } elseif ($lignefraisforfait instanceof ObjectCollection) {
            return $this
                ->useLignefraisforfaitQuery()
                ->filterByPrimaryKeys($lignefraisforfait->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLignefraisforfait() only accepts arguments of type \WsGsb\Model\Lignefraisforfait or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Lignefraisforfait relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFraisforfaitQuery The current query, for fluid interface
     */
    public function joinLignefraisforfait($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Lignefraisforfait');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Lignefraisforfait');
        }

        return $this;
    }

    /**
     * Use the Lignefraisforfait relation Lignefraisforfait object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \WsGsb\Model\LignefraisforfaitQuery A secondary query class using the current class as primary query
     */
    public function useLignefraisforfaitQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLignefraisforfait($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Lignefraisforfait', '\WsGsb\Model\LignefraisforfaitQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildFraisforfait $fraisforfait Object to remove from the list of results
     *
     * @return $this|ChildFraisforfaitQuery The current query, for fluid interface
     */
    public function prune($fraisforfait = null)
    {
        if ($fraisforfait) {
            $this->addUsingAlias(FraisforfaitTableMap::COL_IDFRAISFORFAIT, $fraisforfait->getIdfraisforfait(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the fraisforfait table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FraisforfaitTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            FraisforfaitTableMap::clearInstancePool();
            FraisforfaitTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FraisforfaitTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(FraisforfaitTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            FraisforfaitTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            FraisforfaitTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // FraisforfaitQuery
