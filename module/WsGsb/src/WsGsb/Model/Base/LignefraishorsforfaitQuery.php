<?php

namespace WsGsb\Model\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use WsGsb\Model\Lignefraishorsforfait as ChildLignefraishorsforfait;
use WsGsb\Model\LignefraishorsforfaitQuery as ChildLignefraishorsforfaitQuery;
use WsGsb\Model\Map\LignefraishorsforfaitTableMap;

/**
 * Base class that represents a query for the 'lignefraishorsforfait' table.
 *
 *
 *
 * @method     ChildLignefraishorsforfaitQuery orderByIdfichefrais($order = Criteria::ASC) Order by the idFicheFrais column
 * @method     ChildLignefraishorsforfaitQuery orderByIdlignefraishorsforfait($order = Criteria::ASC) Order by the idLigneFraisHorsForfait column
 * @method     ChildLignefraishorsforfaitQuery orderByDate($order = Criteria::ASC) Order by the date column
 * @method     ChildLignefraishorsforfaitQuery orderByMontant($order = Criteria::ASC) Order by the montant column
 * @method     ChildLignefraishorsforfaitQuery orderByLibelle($order = Criteria::ASC) Order by the libelle column
 *
 * @method     ChildLignefraishorsforfaitQuery groupByIdfichefrais() Group by the idFicheFrais column
 * @method     ChildLignefraishorsforfaitQuery groupByIdlignefraishorsforfait() Group by the idLigneFraisHorsForfait column
 * @method     ChildLignefraishorsforfaitQuery groupByDate() Group by the date column
 * @method     ChildLignefraishorsforfaitQuery groupByMontant() Group by the montant column
 * @method     ChildLignefraishorsforfaitQuery groupByLibelle() Group by the libelle column
 *
 * @method     ChildLignefraishorsforfaitQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildLignefraishorsforfaitQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildLignefraishorsforfaitQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildLignefraishorsforfaitQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildLignefraishorsforfaitQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildLignefraishorsforfaitQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildLignefraishorsforfaitQuery leftJoinFichefrais($relationAlias = null) Adds a LEFT JOIN clause to the query using the Fichefrais relation
 * @method     ChildLignefraishorsforfaitQuery rightJoinFichefrais($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Fichefrais relation
 * @method     ChildLignefraishorsforfaitQuery innerJoinFichefrais($relationAlias = null) Adds a INNER JOIN clause to the query using the Fichefrais relation
 *
 * @method     ChildLignefraishorsforfaitQuery joinWithFichefrais($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Fichefrais relation
 *
 * @method     ChildLignefraishorsforfaitQuery leftJoinWithFichefrais() Adds a LEFT JOIN clause and with to the query using the Fichefrais relation
 * @method     ChildLignefraishorsforfaitQuery rightJoinWithFichefrais() Adds a RIGHT JOIN clause and with to the query using the Fichefrais relation
 * @method     ChildLignefraishorsforfaitQuery innerJoinWithFichefrais() Adds a INNER JOIN clause and with to the query using the Fichefrais relation
 *
 * @method     \WsGsb\Model\FichefraisQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildLignefraishorsforfait findOne(ConnectionInterface $con = null) Return the first ChildLignefraishorsforfait matching the query
 * @method     ChildLignefraishorsforfait findOneOrCreate(ConnectionInterface $con = null) Return the first ChildLignefraishorsforfait matching the query, or a new ChildLignefraishorsforfait object populated from the query conditions when no match is found
 *
 * @method     ChildLignefraishorsforfait findOneByIdfichefrais(int $idFicheFrais) Return the first ChildLignefraishorsforfait filtered by the idFicheFrais column
 * @method     ChildLignefraishorsforfait findOneByIdlignefraishorsforfait(int $idLigneFraisHorsForfait) Return the first ChildLignefraishorsforfait filtered by the idLigneFraisHorsForfait column
 * @method     ChildLignefraishorsforfait findOneByDate(int $date) Return the first ChildLignefraishorsforfait filtered by the date column
 * @method     ChildLignefraishorsforfait findOneByMontant(string $montant) Return the first ChildLignefraishorsforfait filtered by the montant column
 * @method     ChildLignefraishorsforfait findOneByLibelle(string $libelle) Return the first ChildLignefraishorsforfait filtered by the libelle column *

 * @method     ChildLignefraishorsforfait requirePk($key, ConnectionInterface $con = null) Return the ChildLignefraishorsforfait by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLignefraishorsforfait requireOne(ConnectionInterface $con = null) Return the first ChildLignefraishorsforfait matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLignefraishorsforfait requireOneByIdfichefrais(int $idFicheFrais) Return the first ChildLignefraishorsforfait filtered by the idFicheFrais column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLignefraishorsforfait requireOneByIdlignefraishorsforfait(int $idLigneFraisHorsForfait) Return the first ChildLignefraishorsforfait filtered by the idLigneFraisHorsForfait column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLignefraishorsforfait requireOneByDate(int $date) Return the first ChildLignefraishorsforfait filtered by the date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLignefraishorsforfait requireOneByMontant(string $montant) Return the first ChildLignefraishorsforfait filtered by the montant column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLignefraishorsforfait requireOneByLibelle(string $libelle) Return the first ChildLignefraishorsforfait filtered by the libelle column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLignefraishorsforfait[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildLignefraishorsforfait objects based on current ModelCriteria
 * @method     ChildLignefraishorsforfait[]|ObjectCollection findByIdfichefrais(int $idFicheFrais) Return ChildLignefraishorsforfait objects filtered by the idFicheFrais column
 * @method     ChildLignefraishorsforfait[]|ObjectCollection findByIdlignefraishorsforfait(int $idLigneFraisHorsForfait) Return ChildLignefraishorsforfait objects filtered by the idLigneFraisHorsForfait column
 * @method     ChildLignefraishorsforfait[]|ObjectCollection findByDate(int $date) Return ChildLignefraishorsforfait objects filtered by the date column
 * @method     ChildLignefraishorsforfait[]|ObjectCollection findByMontant(string $montant) Return ChildLignefraishorsforfait objects filtered by the montant column
 * @method     ChildLignefraishorsforfait[]|ObjectCollection findByLibelle(string $libelle) Return ChildLignefraishorsforfait objects filtered by the libelle column
 * @method     ChildLignefraishorsforfait[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class LignefraishorsforfaitQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \WsGsb\Model\Base\LignefraishorsforfaitQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'WsGsbConnection', $modelName = '\\WsGsb\\Model\\Lignefraishorsforfait', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildLignefraishorsforfaitQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildLignefraishorsforfaitQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildLignefraishorsforfaitQuery) {
            return $criteria;
        }
        $query = new ChildLignefraishorsforfaitQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildLignefraishorsforfait|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(LignefraishorsforfaitTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = LignefraishorsforfaitTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLignefraishorsforfait A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idFicheFrais, idLigneFraisHorsForfait, date, montant, libelle FROM lignefraishorsforfait WHERE idLigneFraisHorsForfait = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildLignefraishorsforfait $obj */
            $obj = new ChildLignefraishorsforfait();
            $obj->hydrate($row);
            LignefraishorsforfaitTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildLignefraishorsforfait|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildLignefraishorsforfaitQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LignefraishorsforfaitTableMap::COL_IDLIGNEFRAISHORSFORFAIT, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildLignefraishorsforfaitQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LignefraishorsforfaitTableMap::COL_IDLIGNEFRAISHORSFORFAIT, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idFicheFrais column
     *
     * Example usage:
     * <code>
     * $query->filterByIdfichefrais(1234); // WHERE idFicheFrais = 1234
     * $query->filterByIdfichefrais(array(12, 34)); // WHERE idFicheFrais IN (12, 34)
     * $query->filterByIdfichefrais(array('min' => 12)); // WHERE idFicheFrais > 12
     * </code>
     *
     * @see       filterByFichefrais()
     *
     * @param     mixed $idfichefrais The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLignefraishorsforfaitQuery The current query, for fluid interface
     */
    public function filterByIdfichefrais($idfichefrais = null, $comparison = null)
    {
        if (is_array($idfichefrais)) {
            $useMinMax = false;
            if (isset($idfichefrais['min'])) {
                $this->addUsingAlias(LignefraishorsforfaitTableMap::COL_IDFICHEFRAIS, $idfichefrais['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idfichefrais['max'])) {
                $this->addUsingAlias(LignefraishorsforfaitTableMap::COL_IDFICHEFRAIS, $idfichefrais['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LignefraishorsforfaitTableMap::COL_IDFICHEFRAIS, $idfichefrais, $comparison);
    }

    /**
     * Filter the query on the idLigneFraisHorsForfait column
     *
     * Example usage:
     * <code>
     * $query->filterByIdlignefraishorsforfait(1234); // WHERE idLigneFraisHorsForfait = 1234
     * $query->filterByIdlignefraishorsforfait(array(12, 34)); // WHERE idLigneFraisHorsForfait IN (12, 34)
     * $query->filterByIdlignefraishorsforfait(array('min' => 12)); // WHERE idLigneFraisHorsForfait > 12
     * </code>
     *
     * @param     mixed $idlignefraishorsforfait The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLignefraishorsforfaitQuery The current query, for fluid interface
     */
    public function filterByIdlignefraishorsforfait($idlignefraishorsforfait = null, $comparison = null)
    {
        if (is_array($idlignefraishorsforfait)) {
            $useMinMax = false;
            if (isset($idlignefraishorsforfait['min'])) {
                $this->addUsingAlias(LignefraishorsforfaitTableMap::COL_IDLIGNEFRAISHORSFORFAIT, $idlignefraishorsforfait['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idlignefraishorsforfait['max'])) {
                $this->addUsingAlias(LignefraishorsforfaitTableMap::COL_IDLIGNEFRAISHORSFORFAIT, $idlignefraishorsforfait['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LignefraishorsforfaitTableMap::COL_IDLIGNEFRAISHORSFORFAIT, $idlignefraishorsforfait, $comparison);
    }

    /**
     * Filter the query on the date column
     *
     * Example usage:
     * <code>
     * $query->filterByDate(1234); // WHERE date = 1234
     * $query->filterByDate(array(12, 34)); // WHERE date IN (12, 34)
     * $query->filterByDate(array('min' => 12)); // WHERE date > 12
     * </code>
     *
     * @param     mixed $date The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLignefraishorsforfaitQuery The current query, for fluid interface
     */
    public function filterByDate($date = null, $comparison = null)
    {
        if (is_array($date)) {
            $useMinMax = false;
            if (isset($date['min'])) {
                $this->addUsingAlias(LignefraishorsforfaitTableMap::COL_DATE, $date['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($date['max'])) {
                $this->addUsingAlias(LignefraishorsforfaitTableMap::COL_DATE, $date['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LignefraishorsforfaitTableMap::COL_DATE, $date, $comparison);
    }

    /**
     * Filter the query on the montant column
     *
     * Example usage:
     * <code>
     * $query->filterByMontant(1234); // WHERE montant = 1234
     * $query->filterByMontant(array(12, 34)); // WHERE montant IN (12, 34)
     * $query->filterByMontant(array('min' => 12)); // WHERE montant > 12
     * </code>
     *
     * @param     mixed $montant The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLignefraishorsforfaitQuery The current query, for fluid interface
     */
    public function filterByMontant($montant = null, $comparison = null)
    {
        if (is_array($montant)) {
            $useMinMax = false;
            if (isset($montant['min'])) {
                $this->addUsingAlias(LignefraishorsforfaitTableMap::COL_MONTANT, $montant['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($montant['max'])) {
                $this->addUsingAlias(LignefraishorsforfaitTableMap::COL_MONTANT, $montant['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LignefraishorsforfaitTableMap::COL_MONTANT, $montant, $comparison);
    }

    /**
     * Filter the query on the libelle column
     *
     * Example usage:
     * <code>
     * $query->filterByLibelle('fooValue');   // WHERE libelle = 'fooValue'
     * $query->filterByLibelle('%fooValue%', Criteria::LIKE); // WHERE libelle LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libelle The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLignefraishorsforfaitQuery The current query, for fluid interface
     */
    public function filterByLibelle($libelle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libelle)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LignefraishorsforfaitTableMap::COL_LIBELLE, $libelle, $comparison);
    }

    /**
     * Filter the query by a related \WsGsb\Model\Fichefrais object
     *
     * @param \WsGsb\Model\Fichefrais|ObjectCollection $fichefrais The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLignefraishorsforfaitQuery The current query, for fluid interface
     */
    public function filterByFichefrais($fichefrais, $comparison = null)
    {
        if ($fichefrais instanceof \WsGsb\Model\Fichefrais) {
            return $this
                ->addUsingAlias(LignefraishorsforfaitTableMap::COL_IDFICHEFRAIS, $fichefrais->getIdfichefrais(), $comparison);
        } elseif ($fichefrais instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LignefraishorsforfaitTableMap::COL_IDFICHEFRAIS, $fichefrais->toKeyValue('PrimaryKey', 'Idfichefrais'), $comparison);
        } else {
            throw new PropelException('filterByFichefrais() only accepts arguments of type \WsGsb\Model\Fichefrais or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Fichefrais relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLignefraishorsforfaitQuery The current query, for fluid interface
     */
    public function joinFichefrais($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Fichefrais');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Fichefrais');
        }

        return $this;
    }

    /**
     * Use the Fichefrais relation Fichefrais object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \WsGsb\Model\FichefraisQuery A secondary query class using the current class as primary query
     */
    public function useFichefraisQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFichefrais($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Fichefrais', '\WsGsb\Model\FichefraisQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildLignefraishorsforfait $lignefraishorsforfait Object to remove from the list of results
     *
     * @return $this|ChildLignefraishorsforfaitQuery The current query, for fluid interface
     */
    public function prune($lignefraishorsforfait = null)
    {
        if ($lignefraishorsforfait) {
            $this->addUsingAlias(LignefraishorsforfaitTableMap::COL_IDLIGNEFRAISHORSFORFAIT, $lignefraishorsforfait->getIdlignefraishorsforfait(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the lignefraishorsforfait table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LignefraishorsforfaitTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            LignefraishorsforfaitTableMap::clearInstancePool();
            LignefraishorsforfaitTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LignefraishorsforfaitTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(LignefraishorsforfaitTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            LignefraishorsforfaitTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            LignefraishorsforfaitTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // LignefraishorsforfaitQuery
