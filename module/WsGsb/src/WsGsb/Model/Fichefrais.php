<?php

namespace WsGsb\Model;

use WsGsb\Model\Base\Fichefrais as BaseFichefrais;

/**
 * Skeleton subclass for representing a row from the 'fichefrais' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Fichefrais extends BaseFichefrais {

    static function exist($idUser, $moisAnnee) {
        $ff = FichefraisQuery::fetchByIdUserAndMoisAnnee($idUser, $moisAnnee);
        if ($ff != null) {
            return true;
        } else {
            return false;
        }
    }

    public function postSave() {
        if ($this->collLignefraishorsforfaits == null) {
            $collectionFraisForfait = Base\FraisforfaitQuery::create()->find();
            foreach ($collectionFraisForfait as $fraisForfait) {
                $lff = new Lignefraisforfait();
                $lff->setFichefrais($this);
                $lff->setFraisforfait($fraisForfait);
                $lff->setQuantite(0);
                $lff->save();
                $this->addLignefraisforfait($lff);
            }
        }
    }

}  