<?php

namespace WsGsb\Controller;

use Zend\View\Model\JsonModel;
use Zend\Session\Container;
use WsGsb\Model\UserQuery;
use WsGsb\Model\User;
use \Zend\Mvc\Controller\AbstractRestfulController;

class UserController extends AbstractRestfulController {

    public function get($id) {
        $user = UserQuery::create()
                ->findOneByIduser($id);

        return new JsonModel(
                array("data"=>$user->toArray())
        );
    }

}
