<?php

namespace WsGsb\Controller;

use Zend\View\Model\JsonModel;
use Zend\Session\Container;
use WsGsb\Model\EtatQuery;
use WsGsb\Model\Etat;
use \Zend\Mvc\Controller\AbstractRestfulController;

class EtatController extends AbstractRestfulController {

    /**
     * Retourne l'ensemble des types d'état
     * 
     * @return JsonModel Les types d'état au format JSON
     */
    public function getList() {
        $collectionEtat = EtatQuery::create()
                ->find();
        $resultat = ($collectionEtat != null) ? $collectionEtat->toArray():null;
        return new JsonModel(
                array("data" =>$resultat)
        );
    }

    /**
     * Retourne un état à partir d'un identifiant d'état transmis en paramètre
     * 
     * @param type $id
     * @return JsonModel Un état au format JSON
     */
    public function get($id) {
        $etat = EtatQuery::create()
                ->findOneByIdetat($id);
        $resultat = ($etat != null) ? $etat->toArray() : null;
        return new JsonModel(
               array("data" =>$resultat)
        );
    }

}
