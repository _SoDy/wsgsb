<?php

namespace WsGsb\Controller;

use Zend\View\Model\JsonModel;
use Zend\Session\Container;
use WsGsb\Model\Lignefraisforfait;
use WsGsb\Model\LignefraisforfaitQuery;
use \Zend\Mvc\Controller\AbstractRestfulController;

class LignefraisforfaitController extends AbstractRestfulController {

    public function get($id) {
        $collectionLigneFraisForfait = LignefraisforfaitQuery::create()
                ->findByIdfichefrais($id);
        return new JsonModel(
                array("data" => $collectionLigneFraisForfait->toArray())
        );
    }

    public function getList() {
        return new JsonModel(array(
            'data' => 'getList',
        ));
    }

    /**
     * 
     * @param type $id
     * @param type $data
     * @return JsonModel
     * @remarks
     * 
     */
    public function update($id, $data) {
        $ligneFraisForfait = LignefraisforfaitQuery::create()
                ->findOneByArray(
                array(
                    'idFraisForfait' => $data['idFraisForfait'],
                    'idFicheFrais' => $id
                )
        );
        $quantite = $data['quantite'];
        $ligneFraisForfait->setQuantite($quantite);
        $ligneFraisForfait->save();
        return new JsonModel(
                array("data" => $ligneFraisForfait->toArray())
        );
    }

}
