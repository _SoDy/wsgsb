<?php

namespace WsGsb\Controller;

use Zend\View\Model\JsonModel;
use Zend\Session\Container;
use WsGsb\Model\FichefraisQuery;
use WsGsb\Model\Fichefrais;
use \Zend\Mvc\Controller\AbstractRestfulController;

class FichefraisController extends AbstractRestfulController {

    /**
     * Retourne l'ensemble des des fiches de frais de l'utilisateur authentifié
     * 
     * @remark Les fiches de frais incluent les lignes de frais forfaitaires et hors forfait
     * @example /wsgsb/data/documentation/exemples
     * @return JsonModel Les fiches de frais en format JSON
     */
    public function getList() {
        error_reporting(0);
        $container = new Container('utilisateur');
        $listeFicheFrais = FichefraisQuery::create()
                ->LeftJoinWithLignefraisforfait()
                ->leftJoinWithLignefraishorsforfait()
                ->findByIdvisiteur($container->client->getIdUser());

        $listeFicheFrais = $listeFicheFrais->toArray();
        return new JsonModel(array("data" => $listeFicheFrais));
    }

    /**
     * Modifie une fiche de frais à partir des données transmises en paramètres
     * 
     *  
     * @param QueryParameter $id le numéro (ou identififiant) d'une fiche de frais
     * @param FormParameter $data les données à modifier [nbJustificatifs;montantValide;idEtat] et l'identifiant visiteur [idVisiteur]
     * @remarks Profil Comptable uniquement ; L'identifiant du Visiteur est obligatoire ; Seules les données transmises entrainent une mise à jour des champs concernés
     */
    public function update($id, $data) {
        error_reporting(0);
        $container = new Container('utilisateur');
        $profil = $container->client->getProfil();
        if ($profil == 'Visiteur') {
            $resultat = array('Modifications Impossibles');
        } else {
            $ficheFrais = FichefraisQuery::create()
                    ->findOneByArray(
                    array(
                        "idVisiteur" => $data["idVisiteur"],
                        "idFicheFrais" => $id)
            );
            $nbJustificatifs = (isset($data["nbJustificatifs"])) ? $data["nbJustificatifs"] : NULL;
            $montantValide = (isset($data["montantValide"])) ? $data["montantValide"] : NULL;
            $idEtat = (isset($data["idEtat"])) ? $data["idEtat"] : 'EC';
            $ficheFrais->setDatemodif(time());
            $ficheFrais->setNbjustificatifs($nbJustificatifs);
            $ficheFrais->setMontantvalide($montantValide);
            $ficheFrais->setIdetat($idEtat);

            $resultat = $ficheFrais->toArray();
        }

        return new JsonModel(
                array("data" => $resultat,
                )
        );
    }

    /**
     * Retourne une fiche de frais à partir d'un numéro (ou identifiant) de fiche de frais
     * 
     * @remark Les fiches de frais incluent les lignes de frais forfaitaires et hors forfait
     * @example /wsgsb/data/documentation/exemples
     * 
     * @param type $id Le numéro (ou identifiant) d'une fiche de frais
     * @return JsonModel Une fiche de frais en format JSON
     */
    public function get($id) {
        error_reporting(0);
        $container = new Container('utilisateur');

        $ficheFrais = FichefraisQuery::create()
                ->LeftJoinWithLignefraisforfait()
                ->leftJoinWithLignefraishorsforfait()
                ->filterByIdfichefrais($id)
                ->findByIdvisiteur($container->client->getIdUser());

        $ficheFrais = $ficheFrais->toArray();
//       

        return new JsonModel(
                array(
            "data" => $ficheFrais,
        ));
    }

    /**
     * Créer une fiche de frais à partir des données transmises
     * 
     * @param type $InfoReservation les données transmises en paramètres
     * @return JsonModel Une confirmation au format JSON
     */
    public function create($InfoReservation) {
        error_reporting(0);

        $container = new Container('utilisateur');
        $moisAnnee = $InfoReservation["moisAnnee"];

        $ff = Fichefrais::exist($container->client->getIdUser(), $moisAnnee);

        if (!$ff) {
            $ficheFrais = new Fichefrais();
            $ficheFrais->setMoisannee($moisAnnee);
            $ficheFrais->setIdvisiteur($container->client->getIdUser());
            $ficheFrais->setIdetat("EC");
            $ficheFrais->save();
        }

        $ficheFrais = FichefraisQuery::create()
                ->findByArray(
                array(
                    "idVisiteur" => $container->client->getIdUser(),
                    "moisAnnee" => $moisAnnee,
        ));
        $recapitulatif = $ficheFrais->toArray();

        return new JsonModel(array(
            'data' => $recapitulatif,
        ));
    }

}

//$ficheFrais = FichefraisQuery::create()
//                ->setFormatter('Propel\Runtime\Formatter\ArrayFormatter')
//                ->leftJoin("Lignefraisforfait")
//                ->findOneByArray(
//                array(
//                    "idVisiteur" => $container->client->getIdUser(),
//                    "idFicheFrais" => $id)
//        );
//
//        $ligneFraisForfait = LignefraisforfaitQuery::create()
//                ->setFormatter('Propel\Runtime\Formatter\ObjectFormatter')
//                ->findByArray(array(
//            "idFicheFrais" => $id
//                )
//        );
//
//        $ligneFraishorsForfait = LignefraishorsforfaitQuery::create()
//                ->findByArray(array(
//            "idFicheFrais" => $id
//        ));
//
//        $ficheFrais["Lignefraisforfait"] = $ligneFraisForfait->toArray();
//        $ficheFrais["Lignefraishorsforfait"] = $ligneFraishorsForfait->toArray();