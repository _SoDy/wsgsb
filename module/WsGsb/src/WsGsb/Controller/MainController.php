<?php

namespace WsGsb\Controller;

use Zend\View\Model\JsonModel;
use \Zend\Mvc\Controller\AbstractRestfulController;

class MainController extends AbstractRestfulController {
   
    public function create($data) {
        return new JsonModel(array(
            'f' => 'post',
            'data' => var_dump($data)
        ));
    }

    public function delete($id) {
        return new JsonModel(array(
            'f' => 'delete',
        ));
    }

    public function get($id) {
        return new JsonModel(array(
            'f' => 'get',
        ));
    }

    public function getList() {
        return new JsonModel(array(
            'f' => 'getList',
        ));
    }

}
