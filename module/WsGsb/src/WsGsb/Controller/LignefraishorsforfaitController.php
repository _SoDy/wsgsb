<?php

namespace WsGsb\Controller;

use Zend\View\Model\JsonModel;
use Zend\Session\Container;
use WsGsb\Model\Lignefraishorsforfait;
use WsGsb\Model\LignefraishorsforfaitQuery;
use \Zend\Mvc\Controller\AbstractRestfulController;

class LignefraishorsforfaitController extends AbstractRestfulController {

    public function get($id) {
        $collectionLigneFraisHorsForfait = LignefraishorsforfaitQuery::create()
                ->findByIdfichefrais($id);
        return new JsonModel(
                array("data"=>$collectionLigneFraisHorsForfait->toArray())
        );
    }

    /**
     * 
     * @param type $id
     * @param type $data
     * @return JsonModel
     * @remarks
     * 
     */
    public function delete($id) {
        $ligneFraisHorsForfait = LignefraishorsforfaitQuery::create()
                ->findByIdfichefrais($id);
        
       
        $ligneFraisHorsForfait->delete();
        return new JsonModel(
                array(
                    'data'=>'effectif'
                )                
        );
    }

}


