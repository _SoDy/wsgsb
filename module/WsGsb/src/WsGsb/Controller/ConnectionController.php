<?php

namespace WsGsb\Controller;

use Zend\View\Model\JsonModel;
use WsGsb\Model\UserQuery;
use Zend\Session\Container;
use \Zend\Mvc\Controller\AbstractRestfulController;
/**
 * Gère la connexion au service web
 */
class ConnectionController extends AbstractRestfulController{

    /**
     * Vérifie l'existance du login et mot de passe dans la table utilisateur. Le profil doit être transmis.
     * 
     * @param Array les informations transmises en paramètre (login, mot de passe et profil)
     * @return JsonModel Un message encodé en JSON 
     * connected:{true=1/false=0}
     */
    public function create($data) {

        $resultat = false;
        $login = $data['login'];
        $mdp = $data['mdp'];
        //$cryptPassword = $this->cryptToMySqlPassword($mdp);
        $utilisateur = UserQuery::create()->findOneByArray(
                array(
                    'login' => $login,
                    'mdp' => $mdp,
                )
        );
        if ($utilisateur != null) {
            $container = new Container('utilisateur');
            $container->client = $utilisateur;
            $resultat = true;
        }
        return new JsonModel(array(
            'data' => $resultat
        ));
    }

    private function cryptToMySqlPassword($password) {
        $pass = strtoupper(
                sha1(
                        sha1($password, true)
                )
        );
        $pass = '*' . $pass;
        return $pass;
    }

    /**
     * Déconnecte l'utilisateur courant
     * 
     * @param mixed $id
     * @return JsonModel Un message confirmant la déconnection
     */
    public function delete($id) {
        $session = new Container('utilisateur');
        $session->client = null;
        return new JsonModel(array(
            'data' => $session->client,
        ));
    }

    public function get($id) {
        return new JsonModel(array(
            'data' => 'get',
        ));
   }

    public function getList() {
        return new JsonModel(array(
            'data' => 'getList',
        ));
    }

}
