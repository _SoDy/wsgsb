<?php

namespace WsGsb\Controller;

use Zend\View\Model\JsonModel;
use WsGsb\Model\FraisforfaitQuery;
use WsGsb\Model\Fraisforfait;
use Zend\Session\Container;
use \Zend\Mvc\Controller\AbstractRestfulController;


class FraisforfaitController extends AbstractRestfulController {

    public function getList() {
        
         $collectionFraisForfait = 
                FraisforfaitQuery::create()
                ->find();
          return new JsonModel(
            array("data"=>$collectionFraisForfait->toArray())    
        );
    }
    
    public function get($id) {
         $collectionFraisForfait = 
                FraisforfaitQuery::create()
                ->find();
          return new JsonModel(
            array("data"=>$collectionFraisForfait->toArray())    
        );
    }

    

}
