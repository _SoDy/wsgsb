
# Web Service GSB

Web service pour la gestion des fiches de frais

## Installation
------------

```bash
git clone https://_SoDy@bitbucket.org/_SoDy/wsgsb.git

see /wsgsb/data for db configuration (dsn,login & password), propel etc.

```bash
composer install ...vendor dependency directory appears

see https://github.com/zendframework/zend-session/issues/74

## Setup virtual host (Apache)

    <VirtualHost *:80>
        ServerName ws.gsb.com
        DocumentRoot /var/www/html/wsgsb/public
        SetEnv APPLICATION_ENV "development"
        <Directory /var/www/html/wsgsb/public>
            DirectoryIndex index.php
            AllowOverride All
            Order allow,deny
            Allow from all
        </Directory>
    </VirtualHost>
