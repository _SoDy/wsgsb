<?php

return [
    'propel' => [
        'database' => [
            'connections' => [
                'WsGsbConnection' => [
                    'adapter' => 'mysql',
                    'classname' => 'Propel\Runtime\Connection\ConnectionWrapper',
                    'dsn' => 'mysql:host=localhost;dbname=gsb',
                    'user' => 'root',
                    'password' => 'root',
                    'attributes' => []
                ],
            ]
        ],
        'general' => [
            'project' => 'wsGsb'
        ],
        'paths' => [
            'projectDir' => '/var/www/html/wsgsb',
            'schemaDir' => '/var/www/html/wsgsb/script',
            'phpDir' => '/var/www/html/wsgsb/module/WsGsb/src'
        ],
        'runtime' => [
            'defaultConnection' => 'WsGsbConnection',
            'connections' => ['WsGsbConnection']
        ],
        'generator' => [
            'defaultConnection' => 'WsGsbConnection',
            'connections' => ['WsGsbConnection']
        ]
    ]
]; 

