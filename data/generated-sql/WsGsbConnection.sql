
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- etat
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `etat`;

CREATE TABLE `etat`
(
    `idEtat` VARCHAR(2) NOT NULL,
    `libelleEtat` VARCHAR(30) NOT NULL,
    PRIMARY KEY (`idEtat`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- fichefrais
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `fichefrais`;

CREATE TABLE `fichefrais`
(
    `idFicheFrais` INTEGER NOT NULL AUTO_INCREMENT,
    `moisAnnee` CHAR(6) NOT NULL,
    `nbJustificatifs` TINYINT,
    `montantValide` DECIMAL,
    `dateModif` DATETIME,
    `idVisiteur` VARCHAR(4) NOT NULL,
    `idEtat` VARCHAR(2) NOT NULL,
    PRIMARY KEY (`idFicheFrais`),
    UNIQUE INDEX `u_ficheFraisMois` (`idFicheFrais`, `moisAnnee`),
    INDEX `fk_visiteur` (`idVisiteur`),
    INDEX `fk_etat` (`idEtat`),
    CONSTRAINT `fk_etat`
        FOREIGN KEY (`idEtat`)
        REFERENCES `etat` (`idEtat`),
    CONSTRAINT `fk_visiteur`
        FOREIGN KEY (`idVisiteur`)
        REFERENCES `user` (`idUser`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- fraisforfait
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `fraisforfait`;

CREATE TABLE `fraisforfait`
(
    `idFraisForfait` VARCHAR(3) NOT NULL,
    `libelleFraisForfait` VARCHAR(20) NOT NULL,
    `montantFraisForfait` DECIMAL(5,2) NOT NULL,
    PRIMARY KEY (`idFraisForfait`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- lignefraisforfait
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `lignefraisforfait`;

CREATE TABLE `lignefraisforfait`
(
    `idFraisForfait` VARCHAR(3) NOT NULL,
    `idFicheFrais` INTEGER(8) NOT NULL,
    `quantite` SMALLINT NOT NULL,
    PRIMARY KEY (`idFraisForfait`,`idFicheFrais`),
    INDEX `fk_ficheFrais` (`idFicheFrais`),
    CONSTRAINT `fk_ficheFrais`
        FOREIGN KEY (`idFicheFrais`)
        REFERENCES `fichefrais` (`idFicheFrais`),
    CONSTRAINT `fk_fraisForfait`
        FOREIGN KEY (`idFraisForfait`)
        REFERENCES `fraisforfait` (`idFraisForfait`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- lignefraishorsforfait
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `lignefraishorsforfait`;

CREATE TABLE `lignefraishorsforfait`
(
    `idFicheFrais` INTEGER NOT NULL,
    `idLigneFraisHorsForfait` INTEGER NOT NULL AUTO_INCREMENT,
    `date` INTEGER NOT NULL,
    `montant` DECIMAL(10,2) NOT NULL,
    `libelle` VARCHAR(100) NOT NULL,
    PRIMARY KEY (`idLigneFraisHorsForfait`),
    INDEX `idFicheFrais` (`idFicheFrais`),
    CONSTRAINT `lignefraishorsforfait_ibfk_1`
        FOREIGN KEY (`idFicheFrais`)
        REFERENCES `fichefrais` (`idFicheFrais`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- user
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user`
(
    `idUser` VARCHAR(4) NOT NULL,
    `nom` VARCHAR(30) NOT NULL,
    `prenom` VARCHAR(30) NOT NULL,
    `login` VARCHAR(20) NOT NULL,
    `mdp` VARCHAR(20) NOT NULL,
    `adresse` VARCHAR(30) NOT NULL,
    `ville` VARCHAR(30) NOT NULL,
    `cp` VARCHAR(5) NOT NULL,
    `dateEmbauche` DATE NOT NULL,
    `profil` enum('Visiteur','Comptable') NOT NULL,
    PRIMARY KEY (`idUser`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
